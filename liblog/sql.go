package liblog

import "github.com/Sirupsen/logrus"

// DebugSQLQuery is used for outputting SQL queries at a DEBUG level
func DebugSQLQuery(logger *logrus.Logger, query string) {
	logger.WithFields(
		logrus.Fields{
			"query": query,
		},
	).Debug("executing sql")
}
