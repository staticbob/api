package libfile

import (
	"os"
	"path/filepath"
)

// GenerateSiteFilePath takes a base, userID and siteID and builds a filepath
// e.g. base=tmp userID=12345 siteID=98765 builds to /tmp/12345/98765/
func GenerateSiteFilePath(base, userID, siteID string) string {
	return filepath.Join(base, userID, siteID)
}

// CheckExistsAndCreate checks to see if a path exists and creates it if not.
func CheckExistsAndCreate(path string) (err error) {
	exists, err := Exists(path)
	if err != nil {
		return err
	}
	if !exists {
		os.Mkdir(path, 0755)
	}
	return nil
}
