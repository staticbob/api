package libfile

import "io"

type GitRepository struct {
	URL string
	Ref string
}

func (gr *GitRepository) Clone(dest io.Writer) error {
	return nil
}

func NewGitRepository(url, ref string) *GitRepository {
	return &GitRepository{
		URL: url,
		Ref: ref,
	}
}

func NewGitRepositoryHead(url string) *GitRepository {
	return NewGitRepository(url, "HEAD")
}
