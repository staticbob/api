package storage

import (
	"bitbucket.org/staticbob/api/env"
	"github.com/Sirupsen/logrus"
)

// Promote is currently a synonym for Sync
func Promote(srcPath, destPath string) error {
	return Sync(srcPath, destPath)
}

// PromoteScratchToArchive promotes a path from scratch storage to archive storage.
func PromoteScratchToArchive(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "scratch",
		"dest": "archive",
		"path": path,
	}).Info("promote")
	return Promote(scratchPath(path), archivePath(path))
}

// PromoteLocalToStaging promotes a path from local storage to staging storage.
func PromoteLocalToStaging(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "local",
		"dest": "staging",
		"path": path,
	}).Info("promote")
	return Promote(localPath(path), stagingPath(path))
}

// PromoteLocalToArchive promotes a path from local storage to archive storage.
func PromoteLocalToArchive(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "local",
		"dest": "archive",
		"path": path,
	}).Info("promote")
	return Promote(localPath(path), archivePath(path))
}

// PromoteLocalToProduction promotes a path from local storage to production storage.
func PromoteLocalToProduction(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "local",
		"dest": "production",
		"path": path,
	}).Info("promote")
	return Promote(localPath(path), productionPath(path))
}

// PromoteStagingToProduction promotes a path from staging storage to production storage.
func PromoteStagingToProduction(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "staging",
		"dest": "production",
		"path": path,
	}).Info("promote")
	return Promote(stagingPath(path), productionPath(path))
}

// PromoteStagingToArchive promotes a path from staging storage to archive storage.
func PromoteStagingToArchive(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "staging",
		"dest": "archive",
		"path": path,
	}).Info("promoting")
	return Promote(stagingPath(path), archivePath(path))
}

// PromoteProductionToArchive promotes a path from production storage to archive storage.
func PromoteProductionToArchive(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "production",
		"dest": "archive",
		"path": path,
	}).Info("promoting")
	return Promote(productionPath(path), archivePath(path))
}

// PromoteArchiveToProduction promotes a path from archive storage to production storage.
func PromoteArchiveToProduction(path string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  "archive",
		"dest": "production",
		"path": path,
	}).Info("promoting")
	return Promote(archivePath(path), productionPath(path))
}
