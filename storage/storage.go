package storage

import (
	"fmt"
	"os/exec"
	"path/filepath"

	"bitbucket.org/staticbob/api/env"
)

/*
 * Whilst we can use the rclone code (it's written in Go) to avoid breaking
 * changes for now we will just call out to the command line.
 */
var (
	rcloneCommand = "rclone"
	excludeGitArg = []string{"--exclude", ".git*"}
)

func init() {
	// Check `rclone` is installed
	_, err := exec.LookPath(rcloneCommand)
	if err != nil {
		env.Log.Fatalf("unable to locate '%s' command: %s", rcloneCommand, err)
	}
}

func buildRcloneURL(configID, bucket, path string) string {
	rcloneURL := ""
	if configID != "" {
		rcloneURL = fmt.Sprintf("%s%s:", rcloneURL, configID)
	}
	if bucket != "" {
		rcloneURL = fmt.Sprintf("%s%s", rcloneURL, bucket)
	}
	rcloneURL = fmt.Sprintf("%s%s", rcloneURL, path)
	return rcloneURL
}

func localPath(path string) string {
	return filepath.Join(env.StoragePathLocal, path)
}

func scratchPath(path string) string {
	return filepath.Join(env.StorageScratchPath, path)
}

func stagingPath(path string) string {
	return buildRcloneURL(env.StorageConfigIDStaging, env.StorageBucketStaging, path)
}

func archivePath(path string) string {
	return buildRcloneURL(env.StorageConfigIDArchive, env.StorageBucketArchive, path)
}

func productionPath(path string) string {
	return buildRcloneURL(env.StorageConfigIDProduction, env.StorageBucketProduction, path)
}

func execRclone(command string, filters, filePaths []string) error {
	// rclone needs to have a specifc order of terms
	// filters command source dest
	args := append(filters, command)
	args = append(args, filePaths...)
	cmd := exec.Command(rcloneCommand, args...)
	return cmd.Run()
}

func execRcloneSync(source, dest string) error {
	filePaths := []string{source, dest}
	// Sync between the source and the destination excluding any git directories.
	return execRclone("sync", excludeGitArg, filePaths)
}

func execRclonePurge(dest string) error {
	args := []string{dest}
	// Purge everything from a path - useful for cleaning up a user leaving.
	return execRclone("purge", []string{}, args)
}
