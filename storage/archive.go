package storage

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/mholt/archiver"
)

// Archive takes a directory source path, zips it up to a temporary zip file
func Archive(srcPath string) (archiveTime time.Time, err error) {
	// Zip up the path to the scratch location with a timestamp
	// /scratchpath/srcPath/timestamp.zip
	archiveTime = time.Now().UTC()
	tempZipPath := fmt.Sprintf("%s.zip", scratchPath(filepath.Join(srcPath, archiveTime.String())))
	err = archiver.Zip(tempZipPath, []string{srcPath})
	if err != nil {
		return
	}
	// Remove scratch path reference
	defer os.Remove(tempZipPath)
	// Upload to Archive storage
	err = PromoteScratchToArchive(tempZipPath)
	if err != nil {
		return
	}
	return
}
