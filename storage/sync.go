package storage

import (
	"bitbucket.org/staticbob/api/env"
	"github.com/Sirupsen/logrus"
)

// Sync uses rclone to sync between two paths. These paths can be local storage
// or config, bucket and paths for cloud storage like S3; Backblaze B2 etc
func Sync(srcPath, destPath string) error {
	env.Log.WithFields(logrus.Fields{
		"src":  srcPath,
		"dest": destPath,
	}).Info("sync")
	return execRcloneSync(srcPath, destPath)
}
