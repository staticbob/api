package app

import (
	"bitbucket.org/staticbob/api/handlers"
	"github.com/gorilla/mux"
)

// Mux returns gorilla mux routes for the app
func (app *App) Mux() *mux.Router {

	router := mux.NewRouter()

	// API routes
	api := router.PathPrefix("/api").Subrouter().StrictSlash(true)
	// V1 API routes
	v1 := api.PathPrefix("/v1").Subrouter()

	// Only return sites owned by the requesting user
	v1.HandleFunc("/sites", handlers.GetSites).Methods("GET")
	// Create a new website (error on duplicate name)
	v1.HandleFunc("/site/meta", handlers.UpsertSiteMeta).Methods("POST", "PATCH")

	v1Site := v1.PathPrefix("/site/{site_id:[0-9]+}").Subrouter()
	// Get info on specific website
	v1Site.HandleFunc("/", handlers.GetSite).Methods("GET")
	// Update an existing site
	// Update site content, this should only be called when a site has been created with the SiteMeta call
	v1Site.HandleFunc("/", handlers.UpdateSiteContent).Methods("POST", "PATCH")
	// Delete a site (remove storage key from bucket but keep backups - in case)
	v1Site.HandleFunc("/", handlers.DeleteSite).Methods("DELETE")

	// Notification stuff
	v1Site.HandleFunc("/notifications", handlers.GetNotifications).Methods("GET")
	v1Site.HandleFunc("/notification", handlers.PostNotification).Methods("POST")
	v1SiteNotification := v1Site.PathPrefix("/notification/{notification_id:[0-9]+}").Subrouter()
	v1SiteNotification.HandleFunc("/", handlers.GetNotification).Methods("GET")
	v1SiteNotification.HandleFunc("/", handlers.PutNotification).Methods("PATCH")
	v1SiteNotification.HandleFunc("/", handlers.DeleteNotification).Methods("DELETE")

	// Auth routes are not protected by Kong API authentication as they're required
	// to login. Unless we use JWT from AngluarJS to login and API keys for the rest?
	v1Auth := v1.PathPrefix("/auth").Subrouter()
	// Register someone new
	v1Auth.HandleFunc("/register", handlers.PostRegister).Methods("POST")
	// Check login details
	v1Auth.HandleFunc("/authenticate", handlers.PostAuthenticate).Methods("POST")
	v1Auth.HandleFunc("/logout", handlers.GetLogout).Methods("GET")

	v1Webhooks := v1.PathPrefix("/webhook").Subrouter()
	// Trigger a deploy from a git repo for a specific site
	v1Webhooks.HandleFunc("/deploy/{deploy_id:[0-9]+}", handlers.PostWebhookDeploy).Methods("POST")

	return router
}
