package app

// NewApp creates our new webapp
func New() *App {
	return &App{}
}

// App is the capsule for our webapp and API
type App struct {
}
