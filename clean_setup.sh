#!/bin/bash

POSTGRES_USER=sbob
POSTGRES_PASSWORD=$POSTGRES_USER

echo "Starting database recreation"
echo ""

# Seup the docker machine
echo "Getting docker info"
eval $(docker-machine env default)
DOCKER_IP=$(docker-machine ip default)

# Destroy the docker container
echo "Destroying old docker instances"
docker stop sbob-postgres kong-database kong
docker rm sbob-postgres kong-database kong

# Recreate the docker container
echo "Creating new docker instances"
docker run \
  --name sbob-postgres \
  -e POSTGRES_USER=$POSTGRES_USER \
  -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  -p 5432:5432 \
  -d \
  postgres:9.5

echo "Sbob database created"

docker run -d --name kong-database \
                -p 5433:5432 \
                -e "POSTGRES_USER=kong" \
                -e "POSTGRES_DB=kong" \
                postgres:9.5

echo "Kong database created"
sleep 10

docker run -d --name kong \
    -e "DATABASE=postgres" \
    --link kong-database:kong-database \
    -p 8000:8000 \
    -p 8443:8443 \
    -p 8001:8001 \
    -p 7946:7946 \
    -p 7946:7946/udp \
    --security-opt seccomp:unconfined \
    mashape/kong:0.8.3

echo "Kong created"

DATABASE_URL=$(echo "$DOCKER_IP\:5432\/sbob")

# Make sure the conf files have the right IP address
echo "Updating configuration files"
sed -i "" -e "s: *flyway\.url.+$:flyway.url = jdbc\:postgresql\:\/\/$DATABASE_URL:" flyway.conf

sed -i "" -e "s:postgresurl\:.+$:postgresurl\: $POSTGRES_USER\:POSTGRES_PASSWORD@$DATABASE_URL?sslmode=disable:" sbob.yaml

# Migrate the database
echo "Migrating database"
flyway migrate

# Copy the static data

echo "todo: static data copy"

# Add the API and plugins to Kong
echo "Configuring Kong"

http -f POST $(docker-machine ip default):8001/apis/ \
  name=sbob \
  upstream_url=http://$(ifconfig en0 | awk '$1 == "inet" {print $2}'):8080 \
  preserve_host=true \
  request_path=/api/v1/

echo "API added"

http -f POST $(docker-machine ip default):8001/apis/sbob/plugins \
  name=key-auth

echo "Key-auth plugin added"

echo "Done"