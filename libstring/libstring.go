// Package libstring provides string related library functions.
package libstring

import (
	"crypto/rand"
)

const (
	minPasswordLength = 8
)

// RandString generates a random string of characters with length `n`
func RandString(n int) string {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"

	var randBytes = make([]byte, n)
	rand.Read(randBytes)

	for i, b := range randBytes {
		randBytes[i] = letters[b%byte(len(letters))]
	}

	return string(randBytes)
}

// IsValidPassword checks rules like not blank, length etc
func IsValidPassword(password string) bool {
	return password != "" &&
		len(password) >= minPasswordLength
}

// IsValidEmail checks the email address submitted is valid
func IsValidEmail(email string) bool {
	return email != ""
}
