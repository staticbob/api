package handlers

type GenericMessage struct {
	Messages []string `json:"messages"`
}

type SuccessMessage struct {
	GenericMessage
}

func NewSuccessMessage(messages ...string) SuccessMessage {
	m := SuccessMessage{}
	m.Messages = messages
	return m
}

type ErrorMessage struct {
	GenericMessage
	ErrorCode int `json:"errorCode"`
}

func NewErrorMessageE(errorCode int, err error) ErrorMessage {
	return NewErrorMessageS(errorCode, err.Error())
}

func NewErrorMessageS(errorCode int, errorMessages ...string) ErrorMessage {
	m := ErrorMessage{}
	m.Messages = errorMessages
	m.ErrorCode = errorCode
	return m
}
