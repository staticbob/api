package handlers

import "net/http"

// GetProfile returns extra information on the logged in user, forming the user profile.
func GetProfile(w http.ResponseWriter, r *http.Request) {

}

// PostProfile creates a new user profile
func PostProfile(w http.ResponseWriter, r *http.Request) {

}

// PutProfile updates an existing user profile
func PutProfile(w http.ResponseWriter, r *http.Request) {

}

// DeleteProfile removes any profile information but not the account itself (username, email and password). That is implemented in DeleteAccount. If DeleteAccount is called it will automatically remove any profile information as well.
func DeleteProfile(w http.ResponseWriter, r *http.Request) {

}
