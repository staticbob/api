package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/mholt/archiver"

	"bitbucket.org/peren/core/libstring"
	"bitbucket.org/staticbob/api/context"
	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/libfile"
	"bitbucket.org/staticbob/api/libhttp"
	"bitbucket.org/staticbob/api/storage"
)

const (
	maxSiteUploads = 3
)

// UpsertSiteMeta is the first stage to creating a new site.
// Requires no info will just create a new siteID if none in request or update
// a site if ID present.
func UpsertSiteMeta(w http.ResponseWriter, r *http.Request) {
	// Get the user info and reject if none present
	user := context.GetUser(r)
	if user == nil {
		NewUnauthrisedErrorResponse(w)
		return
	}

}

// UpdateSiteContent takes a siteID and a location of some content to push
// to a staging URL.
// Content can be in the form of a zip file in multipart/form-data format or JSON
// containing connection info to pull from a Git repository.
func UpdateSiteContent(w http.ResponseWriter, r *http.Request) {
	// Todo: Use server side events here to provide live feedback to the user
	// about the deployment status.
	// Get the user info and reject if none present
	user := context.GetUser(r)
	if user == nil {
		NewUnauthrisedErrorResponse(w)
		return
	}
	// This should have a siteID to update
	siteID := libhttp.ExtractURLIDString(r, "siteID")
	// Check user has permissions to update site content
	// if !user.
	if false {
		NewUnauthrisedErrorResponse(w)
		return
	}

	var siteUploads []Site
	switch contentType := r.Header.Get("Content Type"); {
	// Simplest scenario - This is a zip file
	case contentType == "multipart/form-data":
		siteUploads = NewZipSites(strconv.Itoa(user.ID), siteID, r)
	// Second scenario - This is a Git URL
	case contentType == "application/json":
		siteUploads = NewGitSites(strconv.Itoa(user.ID), siteID, r)
	default:
		// Bad request
		NewBadRequestResponse(w, NewResponseMessageUnsuccessful("Unable to parse upload"))
	}

	// Prepare site information response, just use a map for now.
	siteInfo := make(map[string]interface{})
	siteInfo["success"] = []string{}
	siteInfo["fail"] = []string{}

	for _, site := range siteUploads[:int(math.Min(float64(len(siteUploads)), float64(maxSiteUploads)))] {
		err := site.Deploy()
		if err != nil {
			siteInfo["fail"] = append(siteInfo["fail"].([]string), site.Name())
		} else {
			siteInfo["success"] = append(siteInfo["success"].([]string), site.Name())
		}
	}
	NewSiteCreatedResponse(w, siteInfo)
}

// Site represents a site interface
type Site interface {
	Name() string
	Deploy() error
}

// NewZipSites builds a list of ZipSites from a http.Request
func NewZipSites(userID, siteID string, r *http.Request) []Site {
	sites := []Site{}
	sites = append(sites, NewZipSite(userID, siteID, r))
	return sites
}

// ZipSite is a static site built from a zip upload
type ZipSite struct {
	ID      string
	UserID  string
	srcFile io.Reader
}

// NewZipSite builds a ZipSite struct
func NewZipSite(userID, siteID string, r *http.Request) *ZipSite {
	return &ZipSite{}
}

// Deploy pushes a site to staging, archive
func (zs *ZipSite) Deploy() (err error) {
	return
}

// Name returns the site identifier
func (zs *ZipSite) Name() string {
	return zs.ID
}

// NewGitSites builds a list of Git sites from a http.Request
func NewGitSites(userID, siteID string, r *http.Request) []Site {
	sites := []Site{}
	sites = append(sites, NewGitSite(userID, siteID, r))
	return sites
}

// GitSite represents a site pulled from a Git repository
type GitSite struct {
	ID     string
	UserID string
	repo   string
}

// Deploy pushes a site to staging, archive
func (gs *GitSite) Deploy() (err error) {
	return
}

// Name returns the site identifier
func (gs *GitSite) Name() string {
	return gs.ID
}

// NewGitSite builds a new GitSite struct
func NewGitSite(userID, siteID string, r *http.Request) *GitSite {
	return &GitSite{}
}

// PostSite creates a new StaticSiteRow with the required information
func PostSite(w http.ResponseWriter, r *http.Request) {
	// Verify we have a signed in user
	user := context.GetUser(r)
	if user == nil {
		NewUnauthrisedErrorResponse(w)
		return
	}
	userID := strconv.Itoa(user.ID)

	siteInfo := make(map[string]interface{})

	// Simplest form: take a Git URL or a Zip File only (add meta information later)
	// Base this off the ContentType
	// application/json = git
	// multipart/form-data = zip
	switch contentType := r.Header.Get("Content Type"); {
	case contentType == "application/json":
		// See if the request has a content ID provided by a multipart upload
		decoder := json.NewDecoder(r.Body)
		var siteRequest PostSiteRequest
		err := decoder.Decode(&siteRequest)
		if err != nil {
			NewBadRequestResponse(w, NewResponseMessageUnsuccessful("unable to understand request"))
			return
		}
		tempSiteID := siteRequest.SiteID
		env.Log.Debug(tempSiteID)
		// If it does, a site should be being created and the meta information needs to be added
		if tempSiteID != "" {

		} else {
			// If a content ID doesn't exist handle a git repository
		}
	// Handle a zip upload
	case contentType == "multipart/form-data":
		// Save all zip files into temp location
		zipPaths, failedSites, err := handleZipForm(userID, r)
		if err != nil {
			NewBadRequestResponse(w, nil)
			return
		}
		// Start building siteInfo
		successSites := []map[string]interface{}{}
		for _, path := range zipPaths {
			// Make sure we delete local copies
			defer os.Remove(path)
			// Add a new site to the database and retrieve ID
			siteID := ""
			// For each zip file, extract into local storage
			err := upsertZipSite(userID, siteID, path)
			if err != nil {
				failedSites = append(failedSites, siteID)
			}
			// todo: pull this into a struct
			siteMap := make(map[string]interface{})
			siteMap["site_id"] = siteID
		}
		siteInfo["success"] = successSites
		siteInfo["failed"] = failedSites
		NewSiteCreatedResponse(w, siteInfo)
	default:
		// Bad request
		NewBadRequestResponse(w, NewResponseMessageUnsuccessful("Unable to parse upload"))
	}
}

func upsertZipSite(userID, siteID, pathToZip string) (err error) {
	sitePath := libfile.GenerateSiteFilePath(env.StoragePathLocal, userID, siteID)
	err = archiver.Unzip(pathToZip, sitePath)
	if err != nil {
		return
	}
	// Promote to staging and return URL
	err = storage.PromoteLocalToStaging(sitePath)
	if err != nil {
		return
	}
	// Archive our local site copy
	_, err = storage.Archive(sitePath)
	if err != nil {
		return
	}
	return
}

func handleZipForm(userID string, r *http.Request) (zipPaths, failedSites []string, err error) {
	reader, err := r.MultipartReader()
	if err != nil {
		return
	}
	// Use max 20MB of RAM (1% of a 2GB system)
	form, err := reader.ReadForm(20 * 1024)
	if err != nil {
		return
	}
	// Clean up after ourselves later
	defer form.RemoveAll()
	// Make sure the form is valid
	if err = validateMultiPartForm(form); err != nil {
		return
	}

	for i, site := range form.File["site"] {
		// For each zip file, look up the corresponding name
		providedName := form.Value["siteNames"][i]
		if providedName == "" {
			// Generate a random selection for now
			providedName = libstring.RandString(10)
		}
		s, err := site.Open()
		if err != nil {
			// Keep track of all filenames that failed to upload
			failedSites = append(failedSites, site.Filename)
			continue
		}
		defer s.Close()
		// Build a filepath using the userID, filename and current time.
		outputPath := fmt.Sprintf("%s-%s.zip", filepath.Join(env.StorageScratchPath, userID, site.Filename), time.Now().UTC().String())
		// Write the upload to the scratch path
		outputZipFile, err := os.Create(outputPath)
		if err != nil {
			// Keep track of all filenames that failed to upload
			failedSites = append(failedSites, site.Filename)
			continue
		}
		defer outputZipFile.Close()
		// Write the output out
		_, err = io.Copy(outputZipFile, s)
		if err != nil {
			failedSites = append(failedSites, site.Filename)
			continue
		}
		zipPaths = append(zipPaths, outputPath)
	}
	return
}

// validateMultiPartForm checks the sites provided are valid
func validateMultiPartForm(form *multipart.Form) (err error) {
	// If no sites were uploaded or more than maxSiteUploads then bad request
	numberSites := len(form.File["site"])
	if numberSites == 0 || numberSites > maxSiteUploads {
		err = fmt.Errorf("invalid number of sites: %d sites is out of the range 0 < x <= %d", numberSites, maxSiteUploads)
		return
	}
	// If we don't have the same number of siteNames (can be empty string) as sites, bad request
	if numberSites != len(form.Value["siteNames"]) {
		err = errors.New("not enough site names for sites")
		return
	}
	return
}
