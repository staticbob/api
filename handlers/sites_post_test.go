package handlers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

/**********************
 * POST Site
 *********************/
func TestPostSiteUnauthenticated(t *testing.T) {
	req, err := http.NewRequest("POST", "/api/v1/site", nil)
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(PostSite)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusUnauthorized, writ.Code, "status code should be unautherised")
}

func TestPostSiteBadRequest(t *testing.T) {
	req, err := http.NewRequest("POST", "/api/v1/site", nil)
	req.Header.Set("X-Consumer-Username", "unit-test")
	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(PostSite)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusBadRequest, writ.Code, "status code should be bad request")
}

// Git JSON mocks cloning and creating a site from a git repository
func TestPostSiteGitJSONSuccess(t *testing.T) {
	successJSON := `{gitURL: "test@tethub.com/testrepo"}`
	req, err := http.NewRequest("POST", "/api/v1/site", strings.NewReader(successJSON))

	req.Header.Set("X-Consumer-Username", "unit-test")
	req.Header.Set("Content Type", "application/json")

	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(PostSite)

	handler.ServeHTTP(writ, req)
}

// Zip FormData mocks uploading a zip file
func TestPostSiteZipFormDataSuccess(t *testing.T) {
	req, err := http.NewRequest("POST", "/api/v1/site", nil)
	req.Header.Set("X-Consumer-Username", "unit-test")
	req.Header.Set("Content Type", "multipart/form-data")
	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(PostSite)

	handler.ServeHTTP(writ, req)
}

// Zip JSON mocks receiving an ID of a staged Zip file
func TestPostSiteZipJSONSuccess(t *testing.T) {
	successJSON := `{siteID: "some-test-site-id-possibly-guuid-dont-know"}`
	req, err := http.NewRequest("POST", "/api/v1/site", strings.NewReader(successJSON))

	req.Header.Set("X-Consumer-Username", "unit-test")
	req.Header.Set("Content Type", "application/json")

	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(PostSite)

	handler.ServeHTTP(writ, req)
}
