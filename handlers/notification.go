package handlers

import "net/http"

// GetNotifications returns all notifications associated with a site
func GetNotifications(w http.ResponseWriter, r *http.Request) {

}

// GetNotification gives more information on a specific notification
func GetNotification(w http.ResponseWriter, r *http.Request) {

}

// PostNotification creates a new notification for the given site
func PostNotification(w http.ResponseWriter, r *http.Request) {

}

// PutNotification updates an existing notification
func PutNotification(w http.ResponseWriter, r *http.Request) {

}

// DeleteNotification removes an existing notification
func DeleteNotification(w http.ResponseWriter, r *http.Request) {

}
