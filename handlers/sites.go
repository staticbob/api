package handlers

import (
	"database/sql"
	"net/http"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/staticbob/api/context"
	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/libhttp"
	"bitbucket.org/staticbob/api/models"
)

var (
	// MessageSiteDeletedSuccess is simply used to verify a site was removed. Exported to enable string checking elsewhere.
	// Todo: Is this needed? What about i18n? Driven from elsewhere?
	MessageSiteDeletedSuccess = "Website successfully deleted"
)

// GetSites returns a list of all sites available to the current user.
func GetSites(w http.ResponseWriter, r *http.Request) {
	// We use the `email` as the username for Kong
	username := libhttp.ExtractAuthenticatedUsername(r)
	if username == "" {
		NewUnauthrisedErrorResponse(w)
		return
	}
	siteDB := models.NewStaticSiteTable(env.SQLDB)
	apikey := libhttp.ExtractAPIKey(r)
	// Lookup via APIKey instead of relying on Kong
	sites, err := siteDB.SitesForAPIKey(nil, apikey)
	if err != nil {
		env.Log.WithFields(
			logrus.Fields{
				"email": username,
				"err":   err,
			},
		).Error("Unable to lookup sites")
		NewInternalServerErrorResponse(w, nil)
		return
	}
	// Todo: Pagination using `Link` HTTP Header
	// For now, will presume people aren't managing 50+ sites.
	NewOKJSONResponse(w, sites)
}

// GetSite returns info on a singular website and errors if not available to the current user. e.g. authentication or authorisation error.
func GetSite(w http.ResponseWriter, r *http.Request) {
	user := context.GetUser(r)
	if user == nil {
		NewUnauthrisedErrorResponse(w)
		return
	}
	siteID, err := libhttp.ExtractURLIDInt(r, "site_id")
	if err != nil {
		NewInternalServerErrorResponse(w, map[string]interface{}{
			"error": "incorrect site id",
		})
		return
	}
	// Look it up in the DB
	siteDB := models.NewStaticSiteTable(env.SQLDB)
	site, err := siteDB.SiteForUser(nil, user.ID, siteID)
	if err != nil {
		errLog := env.Log.WithFields(
			logrus.Fields{
				"userID": user.ID,
				"siteID": siteID,
				"err":    err,
			},
		)
		if err == sql.ErrNoRows {
			errLog.Error("not found")
			NewNotFoundResponse(w)
			return
		}
		errLog.Error("Unable to lookup site")
		// Todo: this should return an unautherised if a site exists but is *not* owned by the current user.
		NewInternalServerErrorResponse(w, map[string]string{})
		return
	}
	NewOKJSONResponse(w, site)
}

// DeleteSite removes a StaticSiteRow.
func DeleteSite(w http.ResponseWriter, r *http.Request) {
	NewOKJSONResponse(w, NewSuccessMessage(MessageSiteDeletedSuccess))
}
