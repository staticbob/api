package handlers

// AbstractSiteRequest contains common information between all SiteRequests
type AbstractSiteRequest struct {
}

// POSTSiteRequest provides a destination for a JSON decoder to decode into
type PostSiteRequest struct {
	SiteID string `json:"siteID,omitempty"`
	GitURL string `json:"gitURL,omitempty"`
}

// PostRegisterRequest creates a new user request to decode into
type PostRegisterRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
