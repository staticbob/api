package handlers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/models"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var (
	mocksql        sqlmock.Sqlmock
	siteSQLColumns []string
)

func mockSQLxDB() *sqlx.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	mocksql = mock

	siteSQLColumns = []string{
		"id",
		"account_id",
		"short_code",
		"created_datetime",
		"last_updated_datetime",
	}

	return sqlx.NewDb(db, "sqlmock")
}

func TestMain(m *testing.M) {
	env.Debug = true
	env.Log = logrus.New()
	env.SQLDB = mockSQLxDB()
	defer env.SQLDB.Close()

	os.Exit(m.Run())
}

/**********************
 * GET Sites
 *********************/
func TestGetSitesUnauthenticated(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/sites", nil)
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(GetSites)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusUnauthorized, writ.Code, "status code should be unautherised")
}

func TestGetSitesDBFailure(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/sites", nil)
	req.Header.Set("X-Consumer-Username", "unit-test")
	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(GetSites)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusInternalServerError, writ.Code, "status code should be internal server error")
}

func TestGetSitesSuccessNoSitesFound(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/sites", nil)
	req.Header.Set("X-Consumer-Username", "unit-test")
	req.Header.Set("apikey", "testApiKey")
	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(GetSites)

	mocksql.ExpectQuery(`^SELECT.*`).WithArgs("testApiKey").WillReturnRows(sqlmock.NewRows(siteSQLColumns))

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusOK, writ.Code, "status code should be ok")
	assert.Equal(t, "[]\n", writ.Body.String(), "should be empty json array")
}

func TestGetSitesSuccessSitesFound(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/sites", nil)
	req.Header.Set("X-Consumer-Username", "unit-test")
	req.Header.Set("apikey", "testApiKey")
	if err != nil {
		t.Fatal(err)
	}
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(GetSites)

	currentTime := time.Now()

	rows := sqlmock.NewRows(siteSQLColumns)
	rows.AddRow(100000001, 2000000200, "shrtCde", currentTime, currentTime)

	mocksql.ExpectQuery(`^SELECT.*`).WithArgs("testApiKey").WillReturnRows(rows)

	handler.ServeHTTP(writ, req)

	expectedJSON := []*models.StaticSiteRow{}
	expectedJSON = append(expectedJSON, &models.StaticSiteRow{
		ID:              100000001,
		AccountID:       2000000200,
		ShortCode:       "shrtCde",
		CreatedTime:     currentTime,
		LastUpdatedTime: currentTime,
	})
	actualJSON := []*models.StaticSiteRow{}

	dec := json.NewDecoder(writ.Body)
	err = dec.Decode(&actualJSON)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, http.StatusOK, writ.Code, "status code should be ok")

	assert.Equal(t, expectedJSON, actualJSON, "json not the same")
	assert.True(t, len(actualJSON) == 1, "should have exactly one site")
}

/**********************
 * GET Sites
 *********************/
func TestGetSiteUnauthenticated(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/site/0123456789/", nil)
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(GetSite)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusUnauthorized, writ.Code, "status code should be unautherised")
}

func TestGetSiteBadRequest(t *testing.T) {

}

func TestGetSiteDBFailure(t *testing.T) {

}

func TestGetSiteNotFound(t *testing.T) {

}

func TestGetSiteSuccessSiteFound(t *testing.T) {

}

/**********************
 * PUT Site
 *********************/
func TestPutSiteUnauthenticated(t *testing.T) {
	req, err := http.NewRequest("PUT", "/api/v1/site/0123456789/", nil)
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(PutSite)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusUnauthorized, writ.Code, "status code should be unautherised")
}

/**********************
 * DELETE Site
 *********************/
func TestDeleteSiteUnauthenticated(t *testing.T) {
	req, err := http.NewRequest("DELETE", "/api/v1/site/0123456789/", nil)
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(DeleteSite)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusUnauthorized, writ.Code, "status code should be unautherised")
}

func TestDeleteSiteSuccess(t *testing.T) {
	req, err := http.NewRequest("DELETE", "/api/v1/site/0123456789/", nil)
	req.Header.Set("X-Consumer-Username", "unit-test")
	if err != nil {
		t.Fatal(err)
	}
	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	writ := httptest.NewRecorder()
	handler := http.HandlerFunc(DeleteSite)

	handler.ServeHTTP(writ, req)
	assert.Equal(t, http.StatusOK, writ.Code, "status code should be okay")
}
