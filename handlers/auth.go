package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/kong"
	"bitbucket.org/staticbob/api/libhttp"
	"bitbucket.org/staticbob/api/models"
	"github.com/jmoiron/sqlx"
	"time"
)

var (
	duplicateEmailErr = "pq: duplicate key value violates unique constraint \"account_email_key\""
)

// PostRegister creates a new user account with the sent information
func PostRegister(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var user PostRegisterRequest
	err := decoder.Decode(&user)
	if err != nil {
		NewInternalServerErrorResponse(w, nil)
		return
	}
	// Save user to database
	acc := models.NewAccountTable(env.SQLDB)
	accRow, err := acc.Signup(nil, user.Email, user.Password)
	if err != nil {
		env.Log.WithFields(
			logrus.Fields{
				"err":  err,
				"func": "acc.Signup(nil, user.Email, user.Password)",
			},
		).Error("Unable to register")
		if err.Error() == duplicateEmailErr {
			NewBadRequestResponse(w, map[string]string{
				"message": "account already exists",
			})
			return
		}
		NewInternalServerErrorResponse(w, map[string]string{
			"message": "unable to register",
		})
		return
	}
	// Register with Kong
	consumer := kong.NewConsumer(strconv.Itoa(accRow.ID), user.Email)
	consumer, err = env.KongAPI.CreateConsumer(consumer)
	if err != nil {
		env.Log.WithFields(
			logrus.Fields{
				"err":  err,
				"func": "env.KongAPI.CreateConsumer(consumer)",
			},
		).Error("Unable to register")
		if err == models.InvalidPasswordErr || err == models.InvalidEmailErr {
			NewBadRequestResponse(w, map[string]string{
				"message": err.Error(),
			})
		} else {
			NewInternalServerErrorResponse(w, map[string]string{
				"message": "unable to register",
			})
		}
		return
	}
	// Fetch an API key for the new consumer
	apiKey, err := env.KongAPI.CreateAPIKeyUsername(user.Email)
	if err != nil {
		NewInternalServerErrorResponse(w, map[string]string{
			"message": "unable to register",
		})
		return
	}
	// Save the API key with our new user
	apiKeyTable := models.NewAPIKey(env.SQLDB)
	apiKeyRow := models.NewAPIKeyRow(accRow.ID, apiKey.ID, apiKey.Key, models.KeyTypeSystem, time.Unix(int64(apiKey.CreatedAt)/100, 0))
	_, err = apiKeyTable.InsertAPIKey(nil, apiKeyRow)
	if err != nil {
		env.Log.WithFields(logrus.Fields{
			"err": err,
		}).Error("Unable to save API key to database")
		NewInternalServerErrorResponse(w, map[string]string{
			"message": "unable to sign in",
		})
		return
	}
	// Return redirect URI to root of admin section and the API key to be used
	NewRedirectResponse(w, "/", apiKey.Key)
}

// PostAuthenticate checks a username and password, creating and logging in a user if credentials are valid.
func PostAuthenticate(w http.ResponseWriter, r *http.Request) {
	var transaction *sqlx.Tx

	decoder := json.NewDecoder(r.Body)
	var user PostRegisterRequest
	err := decoder.Decode(&user)
	if err != nil {
		NewInternalServerErrorResponse(w, nil)
		return
	}
	// Check credentials in DB are correct
	acc := models.NewAccountTable(env.SQLDB)
	accRow, err := acc.GetAccountByEmailAndPassword(transaction, user.Email, user.Password)
	if err != nil {
		NewUnauthrisedErrorResponse(w)
		return
	}
	// Generate new API key
	apiKey, err := env.KongAPI.CreateAPIKeyUsername(user.Email)
	if err != nil {
		NewInternalServerErrorResponse(w, nil)
		return
	}
	env.Log.WithFields(logrus.Fields{
		"apikey": *apiKey,
	}).Debug("Fetched API Key")
	// Save this API Key to our database for reference
	apiKeyTable := models.NewAPIKey(env.SQLDB)
	apiKeyRow := models.NewAPIKeyRow(accRow.ID, apiKey.ID, apiKey.Key, models.KeyTypeSystem, time.Unix(int64(apiKey.CreatedAt)/100, 0))
	_, err = apiKeyTable.InsertAPIKey(nil, apiKeyRow)
	if err != nil {
		env.Log.WithFields(logrus.Fields{
			"err": err,
		}).Error("Unable to save API key to database")
		NewInternalServerErrorResponse(w, map[string]string{
			"message": "unable to sign in",
		})
		return
	}
	// Return redirect URI and the API key to be used
	NewRedirectResponse(w, "/", apiKey.Key)
}

// GetLogout ends the current session for a user and requires login
func GetLogout(w http.ResponseWriter, r *http.Request) {
	apiKey := libhttp.ExtractAPIKey(r)
	if apiKey == "" {
		// If, for some reason, there's no API, just redirect
		NewRedirectResponse(w, "/", "")
		return
	}
	apiKeyTable := models.NewAPIKey(env.SQLDB)
	apiKeyRow, err := apiKeyTable.KongAPIKeyRowByKey(nil, apiKey)
	if err != nil {
		env.Log.WithFields(logrus.Fields{
			"key": apiKey,
			"err": err,
		}).Error("Unable to lookup Kong API key")
		// No point in continuing if no apikey row is found
		NewInternalServerErrorResponse(w, nil)
		return
	}
	// Delete the API key from Kong
	err = env.KongAPI.ExpireAPIKey(apiKeyRow.Email, apiKeyRow.ID)
	if err != nil {
		env.Log.WithFields(logrus.Fields{
			"key": apiKey,
			"err": err,
		}).Error("Unable to remove apikey from Kong")
	}
	// Disable the API key from our database
	_, err = apiKeyTable.DisableAPIKeyByKey(nil, apiKeyRow.Key)
	if err != nil {
		env.Log.WithFields(logrus.Fields{
			"key": apiKey,
			"err": err,
		}).Error("Unable to disable API key")
	}
	// Provide URL for angular to redirect
	NewRedirectResponse(w, "/", "")
}
