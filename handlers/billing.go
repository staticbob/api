package handlers

import "net/http"

// GetBilling fetches the billing information connected with a user account.
func GetBilling(w http.ResponseWriter, r *http.Request) {

}

// PostBilling adds new billing information to a user account.
func PostBilling(w http.ResponseWriter, r *http.Request) {

}

// PutBilling updates billing information related to a user account.
func PutBilling(w http.ResponseWriter, r *http.Request) {

}

// DeleteBilling removes all billing information related to a user account.
func DeleteBilling(w http.ResponseWriter, r *http.Request) {

}
