package handlers

import (
	"encoding/json"
	"net/http"
)

// NewResponseMessage outputs a message to be used in a JSON response
func NewResponseMessage(success bool, message string) map[string]interface{} {
	payload := make(map[string]interface{})
	payload["success"] = success
	payload["message"] = message
	return payload
}

// NewResponseMessageUnsuccessful defaults to success=false when creating a response message
func NewResponseMessageUnsuccessful(message string) map[string]interface{} {
	return NewResponseMessage(false, message)
}

// NewResponseMessageSuccessful default to success=true when create a response message
func NewResponseMessageSuccessful(message string) map[string]interface{} {
	return NewResponseMessage(true, message)
}

// NewInternalServerErrorResponse sends an error message JSON response with a 500 error code
func NewInternalServerErrorResponse(w http.ResponseWriter, payload interface{}) {
	NewJSONResponseC(w, http.StatusInternalServerError, payload)
}

// NewBadRequestResponse sends an error message JSON response with a Bad Request error code
func NewBadRequestResponse(w http.ResponseWriter, payload interface{}) {
	NewJSONResponseC(w, http.StatusBadRequest, payload)
}

// NewUnauthrisedErrorResponse sends an unauthorised JSON response with a 401 unauthorised error code
func NewUnauthrisedErrorResponse(w http.ResponseWriter) {
	payload := make(map[string]interface{})
	payload["error"] = "unauthorised"
	NewJSONResponseC(w, http.StatusUnauthorized, payload)
}

// NewNotFoundResponse sends a 404 response with a short, simple error message to confirm
func NewNotFoundResponse(w http.ResponseWriter) {
	payload := map[string]interface{}{
		"error": "not found",
	}
	NewJSONResponseC(w, http.StatusNotFound, payload)
}

// NewRedirectResponse wraps a URL to be redirected to with the attached authorisation key.
func NewRedirectResponse(w http.ResponseWriter, redirectURL, APIKey string) {
	payload := map[string]interface{}{
		"redirect_url": redirectURL,
		"api_key":      APIKey,
	}
	NewOKJSONResponse(w, payload)
}

// NewOKJSONResponse returns a 200 status code JSON response.
func NewOKJSONResponse(w http.ResponseWriter, payload interface{}) {
	NewJSONResponseC(w, http.StatusOK, payload)
}

// NewSiteCreatedResponse returns a 201 status code with the site ID for further use.
func NewSiteCreatedResponse(w http.ResponseWriter, siteInfo map[string]interface{}) {
	NewJSONResponseC(w, http.StatusCreated, siteInfo)
}

// NewJSONResponseC takes a custom responseCode int and sets the Content-Type to "application/json"
func NewJSONResponseC(w http.ResponseWriter, responseCode int, payload interface{}) {
	// Make sure that we can't have `null` being returned from the API
	if payload == nil {
		payload = make(map[string]string)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(responseCode)
	json.NewEncoder(w).Encode(payload)
}
