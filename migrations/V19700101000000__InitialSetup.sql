CREATE TABLE account (
  id                      SERIAL PRIMARY KEY,
  email                   VARCHAR(255) UNIQUE NOT NULL,
  password                VARCHAR(255) NOT NULL,
  enabled                 BOOLEAN NOT NULL,
  created_datetime        TIMESTAMP WITH TIME ZONE DEFAULT (now() at time zone 'utc') NOT NULL,
  last_active_datetime    TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX account_id_idx ON account(id);
CREATE INDEX account_email_idx ON account(email);

CREATE TABLE profile (
  id                      SERIAL PRIMARY KEY,
  account_id              INTEGER REFERENCES account(id),
  first_name              VARCHAR(100) NOT NULL,
  last_name               VARCHAR(100) NOT NULL
);

CREATE INDEX profile_account_id_idx ON profile(account_id);

CREATE TABLE api_keys (
  id                      SERIAL PRIMARY KEY,
  account_id              INTEGER  NOT NULL REFERENCES account(id),
  kong_id                 VARCHAR(100) NOT NULL,
  key                     VARCHAR(100) NOT NULL,
  key_type                VARCHAR(10) NOT NULL,
  enabled                 BOOLEAN NOT NULL,
  created_datetime        TIMESTAMP WITH TIME ZONE DEFAULT (now() at time zone 'utc') NOT NULL,
  last_active_datetime    TIMESTAMP WITH TIME ZONE
);

CREATE INDEX api_keys_account_id_idx ON api_keys(account_id);
CREATE INDEX api_keys_key_idx ON api_keys(key);

CREATE TABLE billing (
  id          SERIAL PRIMARY KEY,
  account_id  INTEGER NOT NULL REFERENCES account(id)
);

CREATE INDEX billing_account_id_idx ON billing(account_id);

CREATE TABLE static_site (
  id                      SERIAL PRIMARY KEY,
  account_id              INTEGER NOT NULL REFERENCES account(id),
  enabled                 BOOLEAN NOT NULL,
  -- short_code is a short URL used for non-cname domains
  short_code              VARCHAR(10) UNIQUE NOT NULL,
  created_datetime        TIMESTAMP WITH TIME ZONE DEFAULT (now() at time zone 'utc') NOT NULL,
  last_updated_datetime   TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX static_site_account_id_idx ON static_site(account_id);

CREATE TABLE cname_aliases (
  id          SERIAL PRIMARY KEY,
  site_id     INTEGER REFERENCES static_site(id),
  cname_alias VARCHAR(511)
);

CREATE INDEX cname_aliases_site_id_idx ON cname_aliases(site_id);

CREATE TABLE notification (
  id            SERIAL PRIMARY KEY,
  site_id       INTEGER REFERENCES static_site(id),
  on_update     BOOLEAN,
  on_delete     BOOLEAN
);

CREATE INDEX notification_site_id_idx ON notification(site_id);

CREATE TABLE email_notification (
  id                  SERIAL PRIMARY KEY,
  notification_id     INTEGER REFERENCES notification(id),
  email_address       VARCHAR(255)
);

CREATE INDEX email_notification_notification_id_idx ON email_notification(notification_id);

CREATE TABLE slack_notification (
  id                  SERIAL PRIMARY KEY,
  notification_id     INTEGER REFERENCES notification(id),
  slack_address       VARCHAR(255)
);

CREATE INDEX slack_notification_notification_id_idx ON slack_notification(notification_id);

CREATE TABLE hipchat_notification (
  id                  SERIAL PRIMARY KEY,
  notification_id     INTEGER REFERENCES notification(id),
  hipchat_address     VARCHAR(255)
);

CREATE INDEX hipchat_notification_notification_id_idx ON hipchat_notification(notification_id);
