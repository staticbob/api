package libhttp

import (
	"net/http"
	"strconv"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"

	"bitbucket.org/staticbob/api/env"
)

const (
	apiKeyIdentifier = "apikey"
)

// ExtractAuthenticatedUserID pulls the user ID from a request if present. This could
// be from a Kong Header or something else.
// An empty userID should imply there is no AuthenticatedUser in this request
func ExtractAuthenticatedUserID(r *http.Request) (userID int) {
	// Kong if authenticated
	userID, err := strconv.Atoi(r.Header.Get("X-Consumer-ID"))
	if err != nil {
		env.Log.WithField("accountId", r.Header.Get("X-Consumer-ID")).Error("Unable to extract userID from request")
		return
	}
	return
}

// ExtractAuthenticatedUsername returns the username string from a request.
func ExtractAuthenticatedUsername(r *http.Request) (username string) {
	username = r.Header.Get("X-Consumer-Username")
	return
}

// ExtractURLIDString returns a string parameter extracted from the URL (if any present)
func ExtractURLIDString(r *http.Request, urlParameter string) (urlID string) {
	// Extract the vars and maybe error
	vars := mux.Vars(r)
	return vars[urlParameter]
}

// ExtractURLIDInt returns an integer parameter extracted from the URL
func ExtractURLIDInt(r *http.Request, urlParameter string) (urlID int, err error) {
	// Extract the vars and maybe error
	urlIDString := ExtractURLIDString(r, urlParameter)
	urlID, err = strconv.Atoi(urlIDString)
	if err != nil {
		env.Log.WithFields(
			logrus.Fields{
				"urlParameter": urlParameter,
				"urlId":        urlIDString,
				"err":          err,
			},
		).Error("unable to parse id")
		return
	}
	return
}

// ExtractAPIKey pulls the APIKey string from a request returning an empty string if unavailable
func ExtractAPIKey(r *http.Request) string {
	// First try and extract from query string
	apiKeyStr := r.URL.Query().Get(apiKeyIdentifier)
	if apiKeyStr == "" {
		// Then try extracting from header
		apiKeyStr = r.Header.Get(apiKeyIdentifier)
	}
	env.Log.WithFields(logrus.Fields{
		"apikey": apiKeyStr,
		"func":   "libhttp.ExtractAPIKey",
	}).Debug("Extracted APIKey")
	return apiKeyStr
}
