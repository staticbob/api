package context

import (
	"net/http"

	"bitbucket.org/staticbob/api/models"
	"github.com/gorilla/context"
)

// GetUser looks up a user struct from the response else nil
func GetUser(r *http.Request) *models.AccountRow {
	if u, ok := context.GetOk(r, userContextKey); ok {
		return u.(*models.AccountRow)
	}
	return nil
}

// GetSite looks up a static site struct from the response else nil
func GetSite(r *http.Request) *models.StaticSiteRow {
	if s, ok := context.GetOk(r, siteContextKey); ok {
		return s.(*models.StaticSiteRow)
	}
	return nil
}
