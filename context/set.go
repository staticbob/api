package context

import (
	"net/http"

	"bitbucket.org/staticbob/api/models"

	"github.com/gorilla/context"
)

// SetUser forces the right struct type to be set to the request context
func SetUser(r *http.Request, a *models.AccountRow) {
	context.Set(r, userContextKey, a)
}

// SetSite forces the right struct type to be set to the request context
func SetSite(r *http.Request, s *models.StaticSiteRow) {
	context.Set(r, siteContextKey, s)
}
