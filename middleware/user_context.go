package middleware

import (
	"net/http"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/staticbob/api/context"
	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/handlers"
	"bitbucket.org/staticbob/api/libhttp"
	"bitbucket.org/staticbob/api/models"
)

// UserContext builds on gorilla context trying to pull user
// info out of the request and adding it to the context.
func UserContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accountTable := models.NewAccountTable(env.SQLDB)
		userID := int64(libhttp.ExtractAuthenticatedUserID(r))
		if userID != 0 {
			user, err := accountTable.GetByID(nil, userID)
			if err != nil {
				env.Log.WithFields(logrus.Fields{
					"middleware": true,
					"userID":     userID,
					"func":       "UserContext",
					"err":        err,
				}).Debug("unable to lookup user for request")
			} else {
				context.SetUser(r, user)
			}
		}
		next.ServeHTTP(w, r)
	})
}

// Unauthenticated checks the request context for valid user info and
// returns an Unauthorised Error response
func Unauthenticated(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if user := context.GetUser(r); user == nil {
			handlers.NewUnauthrisedErrorResponse(w)
			return
		}
		next.ServeHTTP(w, r)
	})
}
