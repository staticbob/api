package middleware

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/staticbob/api/env"
	"github.com/Sirupsen/logrus"
)

// Timing calculates how long it takes to process a request.
func Timing(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Our middleware logic goes here...
		startTime := time.Now().UTC()
		next.ServeHTTP(w, r)
		endTime := time.Now().UTC()

		env.Log.WithFields(
			logrus.Fields{
				// "startTime": startTime,
				// "endTime":   endTime,
				"totalTime": fmt.Sprintf("%.2fms", endTime.Sub(startTime).Seconds()*1000),
				"url":       r.RequestURI,
			},
		).Debug("request processing time")
	})
}
