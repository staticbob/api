package middleware

import (
	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/libhttp"
	"bitbucket.org/staticbob/api/models"
	"net/http"
)

// APIKeyLastUsed tries to extract an API key from the request and concurrently
// updates the time last accessed.
func APIKeyLastUsed(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		apiKeyStr := libhttp.ExtractAPIKey(r)
		if apiKeyStr != "" {
			// Only try and hit the database if we actually extract some form of api key
			go func(apiKey string) {
				kongAPIKeyTable := models.NewAPIKey(env.SQLDB)
				_, _ = kongAPIKeyTable.UpdateLastActiveNowKey(nil, apiKey)
			}(apiKeyStr)
		}
		next.ServeHTTP(w, r)
	})
}
