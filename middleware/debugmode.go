package middleware

import (
	"net/http"
	"strconv"

	"bitbucket.org/staticbob/api/env"
)

// DebugMode allows the setting of debug mode for a request by setting a `debug=true` query parameter
// This should *only* be allowed by authorised IP addresses
func DebugMode(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		debugModeEnabled, err := strconv.ParseBool(r.URL.Query().Get("debug"))
		if err != nil && debugModeEnabled && !env.Debug {
			// Only enable and disable debug mode if it's disabled already
			env.Debug = true
			next.ServeHTTP(w, r)
			env.Debug = false
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
