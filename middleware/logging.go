package middleware

import (
	"net/http"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/staticbob/api/env"
)

// RouteLogging simply logs the route
func RouteLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		env.Log.WithFields(logrus.Fields{
			"method": r.Method,
			"route":  r.RequestURI,
		}).Debug()
		next.ServeHTTP(w, r)
	})
}

// HeaderLogging dumps out all headers of the request for easy viewing
func HeaderLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := env.Log
		for k, v := range r.Header {
			logger.WithField(k, v)
		}
		logger.Debug(r.Header)
		next.ServeHTTP(w, r)
	})
}
