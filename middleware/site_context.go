package middleware

import (
	"net/http"

	"github.com/Sirupsen/logrus"

	"bitbucket.org/staticbob/api/context"
	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/libhttp"
	"bitbucket.org/staticbob/api/models"
)

// SiteContext tries to add site context to a request only if it's owned by
// the current user. Otherwise Unauthenticated.
func SiteContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
		return
		// Todo: Finish this
		siteID, err := libhttp.ExtractURLIDInt(r, "site_id")
		if err != nil {
			env.Log.WithFields(logrus.Fields{
				"middleware": true,
				"func":       "SiteContext",
				"err":        err,
			}).Debug("unable to extract siteID")
		} else {
			// Check we have a user
			account := context.GetUser(r)
			if siteID != 0 {
				siteTable := models.NewStaticSiteTable(env.SQLDB)
				site, err := siteTable.SiteForUser(nil, account.ID, siteID)
				if err != nil {

				} else {
					context.SetSite(r, site)
				}
			}
		}
		next.ServeHTTP(w, r)
	})
}
