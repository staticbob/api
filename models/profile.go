package models

import (
	"fmt"

	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/liblog"

	"github.com/jmoiron/sqlx"
)

// ProfileRow is a row in the "profile" table
type ProfileRow struct {
	ID        int    `json:"id" db:"id"`
	AccountID int    `json:"accountId" db:"account_id"`
	FirstName string `json:"first_name" db:"first_name"`
	LastName  string `json:"last_name" db:"last_name"`
}

// NewProfile creates our profiles interactor
func NewProfileTable(db *sqlx.DB) *ProfileTable {
	profile := &ProfileTable{}
	profile.db = db
	profile.table = "profile"
	profile.hasID = true

	return profile
}

// Profile interacts with the "profile" table.
type ProfileTable struct {
	Base
}

// AllProfilesWhere fetches profiles based on an optional WHERE clause
func (pro *ProfileTable) AllProfilesWhere(tx *sqlx.Tx, where string, args ...interface{}) ([]*ProfileRow, error) {
	profiles := []*ProfileRow{}
	query := fmt.Sprintf("SELECT * FROM %s", pro.table)
	if where != "" {
		query = fmt.Sprintf("%s WHERE %s", query, where)
	}
	liblog.DebugSQLQuery(env.Log, query)
	err := pro.db.Select(&profiles, query, args...)

	return profiles, err
}
