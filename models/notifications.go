package models

type Contact interface {
	Notify(payload Payload) error
}

type EmailRow struct {
	ContactName  string `json:"contactName"`
	EmailAddress string `json:"emailAddress"`
}

func (email EmailRow) Notify(payload Payload) (err error) {
	return
}

type SlackRow struct {
	SlackAddress string `json:"slackAddress"`
}

func (slack SlackRow) Notify(payload Payload) (err error) {
	return
}

type Payload struct {
	Message string
	Cause   string
}

func NewPayload(message, cause string) Payload {
	return Payload{
		Message: message,
		Cause:   cause,
	}
}

type Notification struct {
	Contact  []Contact
	OnDeploy bool
	OnDelete bool
}
