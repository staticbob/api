package models

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/liblog"
	"bitbucket.org/staticbob/api/libstring"

	"golang.org/x/crypto/bcrypt"

	"github.com/jmoiron/sqlx"
)

var (
	// InvalidPasswordErr is returned when a password does not pass validation
	InvalidPasswordErr = errors.New("Invalid password. Password cannot be blank and must be at least 8 characters.")
	// InvalidEmailErr is returned when an email does not pass validation
	InvalidEmailErr = errors.New("Invalid email. Email cannot be blank.")
)

// NewAccount utilises a sqlx.DB to interact with the "account" table
func NewAccountTable(db *sqlx.DB) *AccountTable {
	account := &AccountTable{}
	account.db = db
	account.table = "account"
	account.hasID = true

	return account
}

// Account represents the "account" table in the database.
type AccountTable struct {
	Base
}

func (acc *AccountTable) accountRowFromSQLResult(tx *sqlx.Tx, sqlResult sql.Result) (*AccountRow, error) {
	accID, err := sqlResult.LastInsertId()
	if err != nil {
		return nil, err
	}

	return acc.GetByID(tx, accID)
}

// GetByID returns record by id.
func (acc *AccountTable) GetByID(tx *sqlx.Tx, id int64) (*AccountRow, error) {
	account := &AccountRow{}
	query := fmt.Sprintf("SELECT * FROM %v WHERE id=$1", acc.table)
	liblog.DebugSQLQuery(env.Log, query)
	err := acc.db.Get(account, query, id)

	return account, err
}

// AllAccounts returns all accounts. This will probably never be used unless for administration purposes.
func (acc *AccountTable) AllAccounts(tx *sqlx.Tx) ([]*AccountRow, error) {
	accounts := []*AccountRow{}
	query := fmt.Sprintf("SELECT * FROM %v", acc.table)
	liblog.DebugSQLQuery(env.Log, query)
	err := acc.db.Select(&accounts, query)

	return accounts, err
}

// GetByEmail returns record by email.
func (acc *AccountTable) GetByEmail(tx *sqlx.Tx, email string) (*AccountRow, error) {
	account := &AccountRow{}
	query := fmt.Sprintf("SELECT * FROM %v WHERE email=$1", acc.table)
	liblog.DebugSQLQuery(env.Log, query)
	err := acc.db.Get(account, query, email)

	return account, err
}

// GetAccountByEmailAndPassword returns record by email but checks password first.
func (acc *AccountTable) GetAccountByEmailAndPassword(tx *sqlx.Tx, email, password string) (*AccountRow, error) {
	account, err := acc.GetByEmail(tx, email)
	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(password))
	if err != nil {
		return nil, err
	}

	return account, err
}

// Signup create a new record of account.
func (acc *AccountTable) Signup(tx *sqlx.Tx, email, password string) (*AccountRow, error) {
	if !libstring.IsValidEmail(email) {
		return nil, InvalidEmailErr
	}
	if !libstring.IsValidPassword(password) {
		return nil, InvalidPasswordErr
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return nil, err
	}

	data := make(map[string]interface{})
	data["email"] = email
	data["password"] = hashedPassword
	data["enabled"] = true

	sqlResult, err := acc.InsertIntoTable(tx, data)
	if err != nil {
		return nil, err
	}

	return acc.accountRowFromSQLResult(tx, sqlResult)
}

// UpdateEmailAndPasswordByID updates account email and password.
func (acc *AccountTable) UpdateEmailAndPasswordByID(tx *sqlx.Tx, accountID int64, email, password, passwordAgain string) (*AccountRow, error) {
	data := make(map[string]interface{})

	if email != "" {
		data["email"] = email
	}

	if password != "" && passwordAgain != "" && password == passwordAgain {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 5)
		if err != nil {
			return nil, err
		}

		data["password"] = hashedPassword
	}

	if len(data) > 0 {
		_, err := acc.UpdateByID(tx, data, accountID)
		if err != nil {
			return nil, err
		}
	}

	return acc.GetByID(tx, accountID)
}

// AccountRow contains the basic access, contact and billing data for a
// account account.
//
// Password is hashed with scrypt or bcrypt and should really be salted as well.
//
// Todo: Support BitBucket, Github and Gitlab connect to allow people
// to sign in and import repos really easily.
type AccountRow struct {
	ID             int       `db:"id"`
	EmailAddress   string    `db:"email"`
	Password       []byte    `db:"password" json:"-"`
	Enabled        bool      `db:"enabled" json:"-"`
	CreatedTime    time.Time `db:"created_time" json:"-"`
	LastActiveTime time.Time `db:"last_active_time" json:"last_active_time"`
}

func (acc *AccountRow) SetPassword(password string) {
	// Validate password
	acc.Password = []byte(password)
}

// BillingData contains Stripe Billing and subscription info.
// Todo: attach Stripe Billing data
type BillingData struct {
}
