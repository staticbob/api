package models

import "github.com/jmoiron/sqlx"

// NewCNameTable returns a CNameTable pointer to manage CNameTable Aliases
func NewCNameTable(db *sqlx.DB) *CNameTable {
	cname := &CNameTable{}
	cname.db = db
	cname.table = "cname_aliases"
	cname.hasID = true

	return cname
}

// CNameTable iteracts with the `cname` table
type CNameTable struct {
	Base
}

// CNameRow is a row in the CNameTable table
type CNameRow struct {
	ID     int    `json:"id" db:"id"`
	SiteID int    `json:"site_id" db:"site_id"`
	CName  string `json:"cname" db:"cname_alias"`
}

// CNamesForSite returns all CNameTable Aliases for a given siteID
func (c *CNameTable) CNamesForSite(tx *sqlx.Tx, siteID int) ([]*CNameRow, error) {
	where := "site_id=$1"
	return c.AllCNamesWhere(tx, where, siteID)
}

// AllCNamesWhere uses a where clause to query the CNameTable table
func (c *CNameTable) AllCNamesWhere(tx *sqlx.Tx, where string, args ...interface{}) ([]*CNameRow, error) {
	cnames := []*CNameRow{}
	err := c.SelectByWhere(tx, &cnames, where, args...)
	if err != nil {
		return cnames, err
	}
	return cnames, nil
}
