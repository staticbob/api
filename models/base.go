package models

/* Thank you to github.com/go-bootstrap/go-bootstrap for the design of their
* API data access layer which I have shamelessly copy/pasted below to use the
* same layout and helper functions.
 */

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/liblog"

	"github.com/jmoiron/sqlx"
)

// InsertResult envelopes a lastInsertID and the number of rowsAffected
type InsertResult struct {
	lastInsertID int64
	rowsAffected int64
}

// LastInsertId returns the last inserted id
func (ir *InsertResult) LastInsertId() (int64, error) {
	return ir.lastInsertID, nil
}

// RowsAffected returns the number of rows affected by the last query
func (ir *InsertResult) RowsAffected() (int64, error) {
	return ir.rowsAffected, nil
}

// Base is inherited by all other table structs to provide some useful helper functionality
type Base struct {
	db    *sqlx.DB
	table string
	hasID bool
}

func (b *Base) newTransactionIfNeeded(tx *sqlx.Tx) (*sqlx.Tx, bool, error) {
	var err error
	wrapInSingleTransaction := false

	if tx != nil {
		return tx, wrapInSingleTransaction, nil
	}

	tx, err = b.db.Beginx()
	if err == nil {
		wrapInSingleTransaction = true
	}

	if err != nil {
		return nil, wrapInSingleTransaction, err
	}

	return tx, wrapInSingleTransaction, nil
}

// InsertIntoTable inserts data[key]value to SQL columns, rows
func (b *Base) InsertIntoTable(tx *sqlx.Tx, data map[string]interface{}) (sql.Result, error) {
	if b.table == "" {
		return nil, errors.New("Table must not be empty.")
	}

	tx, wrapInSingleTransaction, err := b.newTransactionIfNeeded(tx)
	if tx == nil {
		return nil, errors.New("Transaction struct must not be empty.")
	}
	if err != nil {
		return nil, err
	}

	var (
		keys        []string
		dollarMarks []string
		values      []interface{}
	)

	loopCounter := 1
	for key, value := range data {
		keys = append(keys, key)
		dollarMarks = append(dollarMarks, fmt.Sprintf("$%v", loopCounter))
		values = append(values, value)

		loopCounter++
	}

	query := fmt.Sprintf(
		"INSERT INTO %v (%v) VALUES (%v)",
		b.table,
		strings.Join(keys, ","),
		strings.Join(dollarMarks, ","))

	result := &InsertResult{}
	result.rowsAffected = 1

	if b.hasID {
		query = query + " RETURNING id"

		var lastInsertID int64

		err = tx.QueryRow(query, values...).Scan(&lastInsertID)
		if err != nil {
			return nil, err
		}

		result.lastInsertID = lastInsertID
	}
	liblog.DebugSQLQuery(env.Log, query)

	if wrapInSingleTransaction == true {
		err = tx.Commit()
	}

	return result, err
}

// UpdateFromTable updates any rows that match `where` with the specified data.
func (b *Base) UpdateFromTable(tx *sqlx.Tx, data map[string]interface{}, where string) (sql.Result, error) {
	var result sql.Result

	if b.table == "" {
		return nil, errors.New("Table must not be empty.")
	}

	tx, wrapInSingleTransaction, err := b.newTransactionIfNeeded(tx)
	if tx == nil {
		return nil, errors.New("Transaction struct must not be empty.")
	}
	if err != nil {
		return nil, err
	}

	var (
		keysWithDollarMarks []string
		values              []interface{}
	)

	loopCounter := 1
	for key, value := range data {
		keysWithDollarMark := fmt.Sprintf("%v=$%v", key, loopCounter)
		keysWithDollarMarks = append(keysWithDollarMarks, keysWithDollarMark)
		values = append(values, value)

		loopCounter++
	}

	query := fmt.Sprintf(
		"UPDATE %v SET %v WHERE %v",
		b.table,
		strings.Join(keysWithDollarMarks, ","),
		where)

	liblog.DebugSQLQuery(env.Log, query)

	result, err = tx.Exec(query, values...)

	if err != nil {
		return nil, err
	}

	if wrapInSingleTransaction == true {
		err = tx.Commit()
	}

	return result, err
}

// UpdateByID updates a specific row specifed by an id.
func (b *Base) UpdateByID(tx *sqlx.Tx, data map[string]interface{}, id int64) (sql.Result, error) {
	var result sql.Result

	if b.table == "" {
		return nil, errors.New("Table must not be empty.")
	}

	tx, wrapInSingleTransaction, err := b.newTransactionIfNeeded(tx)
	if tx == nil {
		return nil, errors.New("Transaction struct must not be empty.")
	}
	if err != nil {
		return nil, err
	}

	var (
		keysWithDollarMarks []string
		values              []interface{}
	)

	loopCounter := 1
	for key, value := range data {
		keysWithDollarMark := fmt.Sprintf("%v=$%v", key, loopCounter)
		keysWithDollarMarks = append(keysWithDollarMarks, keysWithDollarMark)
		values = append(values, value)

		loopCounter++
	}

	// Add id as part of values
	values = append(values, id)

	query := fmt.Sprintf(
		"UPDATE %v SET %v WHERE id=$%v",
		b.table,
		strings.Join(keysWithDollarMarks, ","),
		loopCounter)

	liblog.DebugSQLQuery(env.Log, query)
	result, err = tx.Exec(query, values...)

	if err != nil {
		return nil, err
	}

	if wrapInSingleTransaction == true {
		err = tx.Commit()
	}

	return result, err
}

// UpdateByKeyValueString updates with a key,value WHERE clause.
func (b *Base) UpdateByKeyValueString(tx *sqlx.Tx, data map[string]interface{}, key, value string) (sql.Result, error) {
	var result sql.Result

	if b.table == "" {
		return nil, errors.New("Table must not be empty.")
	}

	tx, wrapInSingleTransaction, err := b.newTransactionIfNeeded(tx)
	if tx == nil {
		return nil, errors.New("Transaction struct must not be empty.")
	}
	if err != nil {
		return nil, err
	}

	var (
		keysWithDollarMarks []string
		values              []interface{}
	)

	loopCounter := 1
	for key, value := range data {
		keysWithDollarMark := fmt.Sprintf("%v=$%v", key, loopCounter)
		keysWithDollarMarks = append(keysWithDollarMarks, keysWithDollarMark)
		values = append(values, value)

		loopCounter++
	}

	// Add value as part of values
	values = append(values, value)

	query := fmt.Sprintf(
		"UPDATE %v SET %v WHERE %v=$%v",
		b.table,
		strings.Join(keysWithDollarMarks, ","),
		key,
		loopCounter)

	liblog.DebugSQLQuery(env.Log, query)

	result, err = tx.Exec(query, values...)

	if err != nil {
		return nil, err
	}

	if wrapInSingleTransaction == true {
		err = tx.Commit()
	}

	return result, err
}

// DeleteFromTable removes all rows matching the whre clause
func (b *Base) DeleteFromTable(tx *sqlx.Tx, where string, args ...interface{}) (sql.Result, error) {
	var result sql.Result

	if b.table == "" {
		return nil, errors.New("Table must not be empty.")
	}

	tx, wrapInSingleTransaction, err := b.newTransactionIfNeeded(tx)
	if tx == nil {
		return nil, errors.New("Transaction struct must not be empty.")
	}
	if err != nil {
		return nil, err
	}

	query := fmt.Sprintf("DELETE FROM %v", b.table)

	if where != "" {
		query = query + " WHERE " + where
	}

	liblog.DebugSQLQuery(env.Log, query)

	result, err = tx.Exec(query, args...)

	if wrapInSingleTransaction == true {
		err = tx.Commit()
	}

	if err != nil {
		return nil, err
	}

	return result, err
}

// DeleteByID takes an ID and removes the matching row from the table.
func (b *Base) DeleteByID(tx *sqlx.Tx, id int64) (sql.Result, error) {
	return b.DeleteFromTable(tx, "id=$1", id)
}

// GetByWhere is a quick helper function to get into a dest interface
func (b *Base) GetByWhere(tx *sqlx.Tx, dest interface{}, where string, args ...interface{}) (err error) {
	query := fmt.Sprintf("SELECT * FROM %s", b.table)
	if where != "" {
		query = fmt.Sprintf(" %s WHERE %s", query, where)
	}
	liblog.DebugSQLQuery(env.Log, query)
	err = b.db.Get(dest, query, args...)
	if err != nil {
		return
	}
	return
}

// SelectByWhere selects into a dest interface
func (b *Base) SelectByWhere(tx *sqlx.Tx, dest interface{}, where string, args ...interface{}) (err error) {
	query := fmt.Sprintf("SELECT * FROM %s", b.table)
	if where != "" {
		query = fmt.Sprintf(" %s WHERE %s", query, where)
	}
	liblog.DebugSQLQuery(env.Log, query)
	err = b.db.Select(dest, query, args...)
	if err != nil {
		return
	}
	return
}
