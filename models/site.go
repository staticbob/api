package models

import (
	"database/sql"
	"fmt"
	"time"

	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/liblog"

	"github.com/jmoiron/sqlx"
)

// NewStaticSiteTable returns our StaticSite to manage static website database entries.
func NewStaticSiteTable(db *sqlx.DB) *StaticSiteTable {
	site := &StaticSiteTable{}
	site.db = db
	site.table = "static_site"
	site.hasID = true

	return site
}

// StaticSiteRow represents a row in the StaticSite table containing all information relevant to the deployment of a static website
type StaticSiteRow struct {
	ID              int       `json:"id" db:"id"`
	AccountID       int       `json:"account_id" db:"account_id"`
	Enabled         bool      `json:"enabled" db:"enabled"`
	ShortCode       string    `json:"short_code" db:"short_code"`
	CreatedTime     time.Time `json:"created_datetime" db:"created_datetime"`
	LastUpdatedTime time.Time `json:"last_updated_datetime" db:"last_updated_datetime"`
}

// ListStaticSites is a wrapper used for returning JSON containing a list of StaticSiteRow's
type ListStaticSites struct {
	Sites []StaticSiteRow `json:"sites"`
}

// StaticSiteTable wraps Base for interacting with the "static_site" table.
type StaticSiteTable struct {
	Base
}

// AllStaticSitesWhere is a useful helper function to filter all static sites
func (stst *StaticSiteTable) AllStaticSitesWhere(tx *sqlx.Tx, where string, args ...interface{}) (sites []*StaticSiteRow, err error) {
	sites = []*StaticSiteRow{}
	err = stst.SelectByWhere(tx, &sites, where, args...)
	return
}

// SitesForUser looks up all the static sites for a specific userID
func (stst *StaticSiteTable) SitesForUser(tx *sqlx.Tx, userID int) ([]*StaticSiteRow, error) {
	return stst.AllStaticSitesWhere(tx, "account_id=$1", userID)
}

// SitesForAPIKey looks up all the static sites for a specific api key
func (stst *StaticSiteTable) SitesForAPIKey(tx *sqlx.Tx, apiKey string) ([]*StaticSiteRow, error) {
	sites := []*StaticSiteRow{}
	query := fmt.Sprintf(`SELECT
    sites.id AS id, sites.account_id, sites.short_code, sites.created_datetime, sites.last_updated_datetime
  FROM
    %s sites
  LEFT JOIN
    api_keys
  ON
    api_keys.account_id = sites.account_id
  WHERE
    api_keys.key=$1
    AND sites.enabled=true
  `, stst.table)
	liblog.DebugSQLQuery(env.Log, query)
	err := stst.db.Select(&sites, query, apiKey)
	if err != nil {
		return nil, err
	}
	return sites, nil
}

// SitesForEmail looks up all the static sites for a specific email address
func (stst *StaticSiteTable) SitesForEmail(tx *sqlx.Tx, email string) (sites []*StaticSiteRow, err error) {
	sites = []*StaticSiteRow{}
	query := fmt.Sprintf(`SELECT
    sites.id AS id, sites.account_id, sites.short_code, sites.created_datetime, sites.last_updated_datetime
  FROM
    %s sites
  LEFT JOIN
    account
  ON
    sites.account_id = account.id
  WHERE
    email=$1
    AND sites.enabled=true
  `, stst.table)
	liblog.DebugSQLQuery(env.Log, query)
	err = stst.db.Select(&sites, query, email)
	if err != nil {
		return
	}
	return
}

// DisableSiteWhere sets the enabled flag to false for a specific where clause.
func (stst *StaticSiteTable) DisableSiteWhere(tx *sqlx.Tx, where string, args ...interface{}) error {
	return nil
}

// GetSiteWhere returns information on a specific static site given by a WHERE clause
func (stst *StaticSiteTable) GetSiteWhere(tx *sqlx.Tx, where string, args ...interface{}) (*StaticSiteRow, error) {
	site := &StaticSiteRow{}
	err := stst.GetByWhere(tx, site, where, args...)
	if err != nil {
		return nil, err
	}
	return site, nil
}

// SiteForUser returns the details of the requested site only if it is owned by the requested user.
func (stst *StaticSiteTable) SiteForUser(tx *sqlx.Tx, userID, siteID int) (site *StaticSiteRow, err error) {
	where := "id=$1 AND account_id=$2"
	site, err = stst.GetSiteWhere(tx, where, siteID, userID)
	if err != nil {
		return
	}
	return
}

// InsertSite inserts a site strut
func (stst *StaticSiteTable) InsertSite(tx *sqlx.Tx, site StaticSiteRow) (sql.Result, error) {
	data := map[string]interface{}{
		"account_id":            site.AccountID,
		"short_code":            site.ShortCode,
		"last_updated_datetime": time.Now().UTC(),
	}
	return stst.InsertIntoTable(tx, data)
}
