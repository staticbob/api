package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/staticbob/api/app"
	"bitbucket.org/staticbob/api/env"
	"bitbucket.org/staticbob/api/middleware"
	"github.com/Sirupsen/logrus"
	"github.com/carbocation/interpose"
	"github.com/carbocation/interpose/adaptors"
	"github.com/codegangsta/negroni"
)

func init() {
	err := env.SetUp()
	// on the off chance that the environment variables cannot be parsed fully, bomb out using the system logger.
	if err != nil {
		log.Fatalf("Unable to start app; %v", err)
	}
}

func main() {
	app := app.New()
	middle := interpose.New()

	// Use the recovery middleware by Negroni
	recovery := negroni.NewRecovery()
	middle.Use(adaptors.FromNegroni(recovery))

	// Add user context
	middle.Use(middleware.UserContext)

	// Update apikey "last used" time
	middle.Use(middleware.APIKeyLastUsed)

	if env.Debug {
		middle.Use(middleware.RouteLogging)
		middle.Use(middleware.HeaderLogging)
		middle.Use(middleware.Timing)
		logrus.SetLevel(logrus.DebugLevel)
	}

	if !env.Debug && env.DynamicDebug {
		// Only enable DynamicDebug mode if Debug is false
		middle.Use(middleware.DebugMode)
	}

	appAddress := fmt.Sprintf(":%d", env.ListenerPort)
	env.Log.Infof("Starting api on '%s'", appAddress)
	if env.Debug {
		env.Log.Debugf("Debugging enabled")
	}

	middle.UseHandler(app.Mux())
	// graceful.Run(appAddress, 10*time.Second, app.mux())
	http.ListenAndServe(appAddress, middle)
}
