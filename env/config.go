package env

import (
	"fmt"

	"github.com/spf13/viper"
)

const (
	configName          = "sbob"
	defaultPassword     = "change_me"
	defaultLogLevel     = "debug"
	defaultListenerPort = 8080
	defaultKongAPIURL   = "http://localhost:8001/"
)

const (
	confLogLevel     = "loglevel"
	confPostgresURL  = "postgresurl"
	confDebug        = "debug"
	confDynamicDebug = "dynamicdebug"
	confListenerPort = "apiport"
	confKongAPIURL   = "kongurl"
)

const (
	defaultStoragePathLocal          = "/tmp/sbob/"
	defaultStorageScratchPath        = "/tmp/"
	defaultStorageConfigIDStaging    = "staging"
	defaultStorageConfigIDArchive    = "archive"
	defaultStorageConfigIDProduction = "production"
)

const (
	confStorageScratchPath        = "storagescratch"
	confStoragePathLocal          = "storagepathlocal"
	confStorageConfigIDStaging    = "configidstaging"
	confStorageConfigIDArchive    = "configidarchive"
	confStorageConfigIDProduction = "configidproduction"
)

var (
	configPaths = []string{
		fmt.Sprintf("/etc/%s/", configName),
		fmt.Sprintf("$HOME/.%s/", configName),
		".",
	}
	defaultPostgresURL = fmt.Sprintf("localhost:5432/%s", configName)
)

func getConfigWithViper() (err error) {

	viper.SetEnvPrefix(configName)
	viper.AutomaticEnv()

	viper.SetConfigName(configName)
	for _, path := range configPaths {
		viper.AddConfigPath(path)
	}

	// Set some sane defaults just in case
	viper.SetDefault(confLogLevel, defaultLogLevel)
	viper.SetDefault(confPostgresURL, defaultPostgresURL)
	viper.SetDefault(confListenerPort, defaultListenerPort)
	viper.SetDefault(confKongAPIURL, defaultKongAPIURL)
	// Storage defaults
	viper.SetDefault(confStorageScratchPath, defaultStorageScratchPath)
	viper.SetDefault(confStoragePathLocal, defaultStoragePathLocal)
	viper.SetDefault(confStorageConfigIDArchive, defaultStorageConfigIDArchive)
	viper.SetDefault(confStorageConfigIDProduction, defaultStorageConfigIDProduction)
	viper.SetDefault(confStorageConfigIDStaging, defaultStorageConfigIDStaging)

	return viper.ReadInConfig()
}
