package env

import (
	"bitbucket.org/staticbob/api/kong"

	"github.com/jmoiron/sqlx"

	"github.com/Sirupsen/logrus"
)

var (
	// SQLDB is the communication layer to the underlying database (currently PostgreSQL)
	SQLDB *sqlx.DB
	// StripeAPIKey to accept payments
	StripeAPIKey string
	// Log - a logrus powered logger for structured logging
	Log *logrus.Logger
	// Debug enables or disables debugging mode
	Debug bool
	// DynamicDebug allows debugging via a `debug=true` query parameter on a request
	DynamicDebug bool
	// ListenerPort is the API port to listen on
	ListenerPort int
	// KongAPI is a Kong API to update consumers/API Keys etc
	KongAPI *kong.API
)

// Storage variables for the Staging, Archive and Production zones.
// The strings represent the name of the section in the `rclone` config.
var (
	StorageConfigIDStaging    string
	StorageConfigIDArchive    string
	StorageConfigIDProduction string
)

var (
	StorageScratchPath      string
	StoragePathLocal        string
	StorageBucketStaging    string
	StorageBucketArchive    string
	StorageBucketProduction string
)
