package env

import (
	"fmt"
	"strings"

	"bitbucket.org/staticbob/api/kong"
	"bitbucket.org/staticbob/api/libfile"

	// The postgresql driver
	_ "github.com/lib/pq"

	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
)

// SetUp sets all the values to be accessed by the project
func SetUp() (err error) {
	// Parse env config
	err = getConfigWithViper()
	if err != nil {
		return
	}
	Debug = viper.GetBool(confDebug)
	DynamicDebug = viper.GetBool(confDynamicDebug)
	ListenerPort = viper.GetInt(confListenerPort)

	setupFuncs := []func() error{
		// Database setup
		setUpDB,
		// Logging setup
		setUpLogging,
		// Storage setup
		setUpStorage,
		// KongAPI setup,
		setUpKong,
	}

	// Turn this into a loop to save boilerplate
	for _, f := range setupFuncs {
		err = f()
		if err != nil {
			return
		}
	}

	return
}

func setUpStorage() (err error) {
	// Setup storage buckets
	StorageBucketStaging = viper.GetString("bucketstaging")
	StorageBucketProduction = viper.GetString("bucketproduction")
	StorageBucketArchive = viper.GetString("bucketarchive")
	// Setup ConfigID's to lookup in rclone config
	StorageConfigIDArchive = viper.GetString(confStorageConfigIDArchive)
	StorageConfigIDProduction = viper.GetString(confStorageConfigIDProduction)
	StorageConfigIDStaging = viper.GetString(confStorageConfigIDStaging)
	// Setup local path
	StoragePathLocal = viper.GetString(confStoragePathLocal)
	err = libfile.CheckExistsAndCreate(StoragePathLocal)
	if err != nil {
		return
	}
	// Setup scratch path for uploading of files
	StorageScratchPath = viper.GetString(confStorageScratchPath)
	err = libfile.CheckExistsAndCreate(StorageScratchPath)
	if err != nil {
		return
	}

	return
}

func setUpLogging() (err error) {
	Log = logrus.New()
	if Debug {
		// If in debug mode use debug logging
		Log.Level = logrus.DebugLevel
		return
	}
	lvl, err := logrus.ParseLevel(strings.ToLower(viper.GetString(confLogLevel)))

	if err != nil {
		return
	}
	Log.Level = lvl
	return
}

func setUpDB() (err error) {
	// Fail on open
	SQLDB = sqlx.MustOpen("postgres", fmt.Sprintf("postgres://%v", viper.GetString(confPostgresURL)))
	if err != nil {
		return
	}
	return
}

func setUpKong() (err error) {
	KongAPI = kong.NewAPI(viper.GetString(confKongAPIURL))
	return
}
