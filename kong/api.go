package kong

import "fmt"

// API creates a base object to interact with the Kong API
type API struct {
	baseURL string
}

func (api *API) buildURL(route string) string {
	return fmt.Sprintf("%s%s", api.baseURL, route)
}

// NewAPI creates a Kong API object with the baseURL set.
func NewAPI(baseURL string) *API {
	return &API{
		baseURL: baseURL,
	}
}
