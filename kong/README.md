# Kong

## Development

### Start Kong datastore

We're going to use PostgreSQL for simplicity

```
docker run -d --name kong-database \
                -p 5433:5432 \
                -e "POSTGRES_USER=kong" \
                -e "POSTGRES_DB=kong" \
                postgres:9.5
```

### Start Kong

```
docker run -d --name kong \
    -e "DATABASE=postgres" \
    --link kong-database:kong-database \
    -p 8000:8000 \
    -p 8443:8443 \
    -p 8001:8001 \
    -p 7946:7946 \
    -p 7946:7946/udp \
    --security-opt seccomp:unconfined \
    mashape/kong:0.8.3
```

### Reload Kong

```
docker exec -it kong kong reload
```

### Add localhost API

```
http -f POST $(docker-machine ip default):8001/apis/ \
  name=sbob \
  upstream_url=http://$(ifconfig en0 | awk '$1 == "inet" {print $2}'):8080 \
  preserve_host=true \
  request_path=/api/v1/
```

### Delete API

```
http DELETE $(docker-machine ip default):8001/apis/sbob/
```

### Add Consumer

Required for KeyAuthentication plugin.

```
http -f POST $(docker-machine ip default):8001/consumers/ \
  username=test@user.com \
  custom_id=5151
```

### Delete Consumer

```
http DELETE $(docker-machine ip default):8001/consumers/test@user.com
```

### Enable KeyAuthentication plugin

```
http -f POST $(docker-machine ip default):8001/apis/sbob/plugins \
  name=key-auth
```

### Generate API key

```
http -f POST $(docker-machine ip default):8001/consumers/test@user.com/key-auth
```

### List API Keys

```
http -f GET $(docker-machine ip default):8001/consumers/test@user.com/key-auth
```
