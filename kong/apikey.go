package kong

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/Sirupsen/logrus"
	"net/http"
)

const (
	pathKeyAuthentication = "/key-auth"
)

// APIKey represents an API key response from the KeyAuthentication plugin of Kong.
// This assumes the KeyAuthentication plugin has been enabled on Kong
type APIKey struct {
	ID         string `json:"id" db:"id"`
	ConsumerID string `json:"consumer_id"`
	CreatedAt  int    `json:"created_at"`
	Key        string `json:"key" db:"key"`
}

// CreateAPIKey requests an API key for a consumer from the KeyAuthentication plugin of Kong
func (api *API) CreateAPIKey(consumer *Consumer) (*APIKey, error) {
	return api.CreateAPIKeyUsername(consumer.GetID())
}

// CreateAPIKeyUsername uses a username to create an APIKey with Kong
func (api *API) CreateAPIKeyUsername(username string) (*APIKey, error) {
	url := api.buildURL(fmt.Sprintf("%s%v%s", routeConsumers, username, pathKeyAuthentication))
	logrus.WithFields(logrus.Fields{
		"url":      url,
		"username": username,
	}).Debug("Creating API Key")
	res, err := http.Post(url, "application/x-www-form-urlencoded;charset=utf-8", nil)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"err":  err,
			"func": "http.Post(url, \"application/x-www-form-urlencoded;charset=utf-8\", nil)",
		}).Error("Unable to get API key from Kong")
		return nil, err
	}
	if res.StatusCode >= 300 {
		return nil, errors.New("Invalid response from Kong")
	}
	// Decode into the apikey
	defer res.Body.Close()
	decoder := json.NewDecoder(res.Body)
	var apikey APIKey
	err = decoder.Decode(&apikey)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"err":  err,
			"func": "decoder.Decode(&apikey)",
		}).Error("Unable to decode response from Kong")
		return nil, err
	}
	logrus.WithFields(logrus.Fields{
		"apikey":   apikey,
		"username": username,
	}).Debug("Got API key from Kong")
	return &apikey, nil
}

// ExpireAPIKey deletes an API Key from the KeyAuthentication plugin in Kong. Used for logouts etc.
func (api *API) ExpireAPIKey(consumerID, apiKeyID string) error {
	url := api.buildURL(fmt.Sprintf("%s%v%s/%s", routeConsumers, consumerID, pathKeyAuthentication, apiKeyID))
	req, err := http.NewRequest("DELETE", url, nil)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded;charset=utf-8")

	_, err = http.DefaultClient.Do(req)
	return err
}

// GetAPIKeys fetch all API keys for a given username
func (api *API) GetAPIKeys(username string) ([]*APIKey, error) {
	var resp struct {
		Total int       `json:"total"`
		Data  []*APIKey `json:"data"`
	}
	url := api.buildURL(fmt.Sprintf("%s%v%s", routeConsumers, username, pathKeyAuthentication))
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json; charset=utf-8")

	res, err := http.DefaultClient.Do(req)
	// Decode into the apikey
	defer res.Body.Close()
	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(&resp)
	if err != nil {
		return nil, err
	}
	return resp.Data, nil
}
