package kong

import (
	"bytes"
	"encoding/json"
	"net/http"
)

const (
	routeConsumers = "/consumers/"
)

// Consumer is a Kong API consumer
type Consumer struct {
	ID        string `json:"id,omitempty"`
	CustomID  string `json:"custom_id,omitempty"`
	Username  string `json:"username,omitempty"`
	CreatedAt int    `json:"created_at,omitempty"`
}

// GetID prefers the CustomID but falls back to ID
func (con *Consumer) GetID() string {
	if con.CustomID != "" {
		return con.CustomID
	}
	return con.ID
}

// NewConsumer creates a consumer from a customID and a username
func NewConsumer(customID, username string) *Consumer {
	return &Consumer{
		CustomID: customID,
		Username: username,
	}
}

// NewConsumerUsername creates a new consumer from a username
func NewConsumerUsername(username string) *Consumer {
	return &Consumer{
		Username: username,
	}
}

// NewConsumerCustomID creates a new consumer from a customID
func NewConsumerCustomID(customid string) *Consumer {
	return &Consumer{
		CustomID: customid,
	}
}

// CreateConsumer takes a simple consumer, hits the kong API returns the populated Consumer or an err.
func (api *API) CreateConsumer(consumer *Consumer) (*Consumer, error) {
	// Marshal JSON
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(consumer)

	// Send the request
	res, err := http.Post(api.buildURL(routeConsumers), "application/json; charset=utf-8", b)
	if err != nil {
		return nil, err
	}
	// Decode into the consumer
	defer res.Body.Close()
	decoder := json.NewDecoder(res.Body)
	err = decoder.Decode(consumer)
	if err != nil {
		return nil, err
	}
	return consumer, nil
}
