package sitemanager

const (
	versionLatest = "latest"
)

type SiteManager struct {
	UserID  string
	SiteID  string
	Version string
}

// Upsert updates an existing site or creates a new one if it does not exist.
func (sm *SiteManager) Upsert(src SiteSource) (err error) {
	return
}

func NewSiteManager(userID, siteID string) *SiteManager {
	return NewSiteManagerV(userID, siteID, versionLatest)
}

func NewSiteManagerV(userID, siteID, version string) *SiteManager {
	return &SiteManager{
		UserID:  userID,
		SiteID:  siteID,
		Version: version,
	}
}
