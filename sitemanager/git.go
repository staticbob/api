package sitemanager

// GitSiteSource manages sites provided by a git URL. This can be a privately
// hosted URL (needs credentials) or could be taken from a Github/BitBucket
// social connected account.
// GitSiteSource implements the SiteSource interface.
type GitSiteSource struct {
	SiteID  string
	RepoURL string
	creds   GitCredentials
}

// Stage clones the git repository into the staging zone
func (gsm *GitSiteSource) Stage() (err error) {
	return
}

// Deploy pushes the code to the correct storage place
func (gsm *GitSiteSource) Deploy() (err error) {
	return
}

// Archive backs up the site to the backup storage
func (gsm *GitSiteSource) Archive() (err error) {
	return
}

// GitCredentials provides information to be able to authenticate with a
// git repository. The recommended way is with a private key.
type GitCredentials struct {
	SSHPrivateKey string
	Username      string
	Password      string
}

// NewGitSiteSourceC creates a new GitSiteSource from a siteID, repository URL and credentials.
func NewGitSiteSourceC(siteID, repoURL string, creds GitCredentials) SiteSource {
	return &GitSiteSource{
		SiteID:  siteID,
		RepoURL: repoURL,
		creds:   creds,
	}
}

// NewGitSiteSource creates a new GitSiteSource from a siteID and repository
// URL providing empty creds
func NewGitSiteSource(siteID, repoURL string) SiteSource {
	return NewGitSiteSourceC(siteID, repoURL, GitCredentials{})
}
