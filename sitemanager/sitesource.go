package sitemanager

// SiteSource provides an interface site data sources.
// This can be through a Git repository; a Zip archive or more
type SiteSource interface {
	Stage() error
	Deploy() error
	Archive() error
}
