package sitemanager

import (
	"path/filepath"
	"strconv"
	"time"
)

// Unzip extracts the src zip file into the dest directory.
func Unzip(src, dest string) (err error) {
	return
}

// ZipSiteSource manages sites provided by a zip path. This can be a cloud
// storage path or a URL (in future)
// ZipSiteSource implements the SiteSource interface.
type ZipSiteSource struct {
	SiteID string
	ZipSrc string
}

// Stage extracts the Zip file into a destination directory made up of the
// siteID and a time.
func (zsm *ZipSiteSource) Stage() (err error) {
	// example dest directory: 123456/
	dest := filepath.Join(zsm.SiteID, strconv.FormatInt(time.Now().Unix(), 10))
	err = Unzip(zsm.ZipSrc, dest)
	if err != nil {
		return
	}
	return
}

// Deploy pushes the code to the correct storage place
func (zsm *ZipSiteSource) Deploy() (err error) {
	return
}

// Archive backs up the site to the backup storage
func (zsm *ZipSiteSource) Archive() (err error) {
	return
}

// NewZipSiteSource creates a new ZipSiteSource from a siteID and path to zip file
func NewZipSiteSource(siteID, pathToZip string) SiteSource {
	return &ZipSiteSource{
		SiteID: siteID,
		ZipSrc: pathToZip,
	}
}
