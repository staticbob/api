# Static Pipeline (Statine)

## Running

### Development

- Get a docker container with PostgreSQL running and note the IP
  - `docker run --name sbob-postgres -e POSTGRES_USER=sbob -e POSTGRES_PASSWORD=sbob -p 5432:5432 -d postgres:9.5`
- Copy the config templates
  - `cp flyway.conf.template flyway.conf`
  - `cp sbob.yaml.template sbob.yaml`
- Update values in both config files
- Migrate database
  - `flyway migrate`
- Copy in static data
  - TODO - copy static data SQL from `dev/data/`

## Testing

### `httpie`

https://github.com/jkbrzt/httpie

Some useful `httpie` commands (assume running on port `8001` - Kong and `8080` - Raw API)

- Register user
  - `http --follow --json POST localhost:8080/api/v1/auth/register/ email=test@email.com password=test1234`
- Log user in
  - `http --follow --json POST localhost:8080/api/v1/auth/authenticate/ email=test@email.com password=test1234`
- Log user out
  - `http --follow --json GET "$(docker-machine ip default):8001/api/v1/auth/logout/?apikey="`
- Get all sites for a user (raw API)
  - `http --follow localhost:8080/api/v1/sites X-Consumer-ID:27`
- Get all sites for a user (Kong)
  - `http --follow "$(docker-machine ip default):8000/api/v1/sites?apikey="`

## Downstream

### Kong

Kong is used to provide API functionality out of the box.

- API Key access
- JWT verification (still requires upstream validation of claims)
- Rate limiting
- Logging

As PostgreSQL powers our API, PostgreSQL is the chosen backend for Kong.

## Design decisions

### Monolith

This repository represents the entire API rather than a specific microservice. This decision was made choosing simplicity and speed over future scalability to build a product that's able to test the market.

### Efficiency

Whilst trying not to over-optimise early some effort has been put in to find, select and implement efficient components that will hopefully delay any major scaling issues for as long as possible (and keep hosting costs low).

### API only

API's are the backbone of the modern internet and this is no exception. Whilst this API provides all the complicated logic the static frontend (written in AngularJS 2) will be deployed, updated and managed by this very system so we are consumers of our own product.
